'use strict';

angular.module('myApp.deck', ['ngRoute','myApp.services','akoenig.deckgrid','ngDragDrop','ui.bootstrap','al-click'])

.config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/deck', {
    templateUrl: 'deck/deck.html',
    controller: 'DeckCtrl'
  });
}])

.controller('DeckCtrl', ['$scope','pokeDexService','$uibModal',function($scope,pokeDexService, $uibModal) {

  $scope.items;
  $scope.search;
  $scope.compareList= [];
  $scope.optionsList;

  $scope.clearCompare = clearCompare;
  $scope.openPokemonDetail = openPokemonDetail;
  $scope.openPokeDetailCompare = openPokeDetailCompare;


  activate();

  /////////////

  $scope.optionsList = {
    accept: function(dragEl) {
      if ($scope.compareList.length >= 2) {
        return false;
      } else {
        return true;
      }
    }
  };

  function activate()
  {
    var query  = pokeDexService.getPokeDex().get();
    query.$promise.then(function(data)
    {
      if(data)
      {
        var pokemons = data.pokemon;
        $scope.items = [];
        pokemons.forEach(function(item,index)
        {
          var temp = item.resource_uri;
          temp = temp.replace('api/v1/pokemon/','');
          temp = temp.replace('/','');
          temp = parseInt(temp);
          if(temp<720)
          {
            item.id = temp;
            item.image_uri = 'http://pokeapi.co/media/img/'+temp+'.png';
            item.index = $scope.items.length;
            $scope.items.push(item);
          }
        });
      }
    });
  }

  $scope.$watch('compareList', function(newVal) {
    if(newVal && newVal.length>1)
    {
      if(newVal[0].id == newVal[1].id)
      {
        $scope.compareList.splice(1, 1);
        alert("No need compare same pokemon.");
      }
    }
  }, true);

  function clearCompare()
  {
    $scope.compareList = [];
  }

  function openPokeDetailCompare(compareList)
  {
    if(compareList && compareList.length>1)
    {
      var modalInstance = $uibModal.open({
        animation: $scope.animationsEnabled,
        templateUrl: 'deck/modals/comparePokemon.html',
        controller: 'comparePokemonModalInstanceCtrl',
        size: 'md',
        resolve: {
          compareList: function () {
            return compareList;
          }
        }
      });
    }
    else
    {
      alert("Nothing to compare.");
    }


  }

  function openPokemonDetail(item)
  {
    if(item)
    {
      var modalInstance = $uibModal.open({
        animation: $scope.animationsEnabled,
        templateUrl: 'deck/modals/pokemonDetail.html',
        controller: 'PokemonDetailModalInstanceCtrl',
        size: 'sm',
        resolve: {
          item: function () {
            return item;
          }
        }
      });
    }
    else
    {
      alert("Error, try again.");
    }
  }



}])

.controller('comparePokemonModalInstanceCtrl', ['$scope', '$uibModalInstance', 'compareList','pokeDexService',function($scope, $uibModalInstance, compareList, pokeDexService) {

  $scope.items = compareList;
  $scope.one;
  $scope.two;
  $scope.oneDesc ;
  $scope.twoDesc ;

  $scope.ok = ok;
  $scope.calcTypeScore = calcTypeScore;

  activate();

  //////////////////////

  var typeCalcMatrix = {

    "NORMAL"    : [100, 100,  100,  100,  100,  100,  100,  100,  100,  100,  100,  100,  50,   0,    100,  100,  50,   100],
    "FIRE"      : [100, 50,   50,   100,  200,  200,  100,  100,  100,  100,  100,  200,  50,   100,  50,   100,  200,  100],
    "WATER"     : [100, 200,  50,   100,  50,   100,  100,  100,  200,  100,  100,  100,  200,  100,  50,   100,  100,  100],
    "ELECTRIC"  : [100, 100,  200,  50,   50,   100,  100,  100,  0,    200,  100,  100,  100,  100,  50,   100,  100,  100],
    "GRASS"     : [100, 50,   200,  100,  50,   100,  100,  50,   200,  50,   100,  50,   200,  100,  50,   100,  50,   100],
    "ICE"       : [100, 50,   50,   100,  200,  50,   100,  100,  200,  200,  100,  100,  100,  100,  200,  100,  50,   100],
    "FIGHTING"  : [200, 100,  100,  100,  100,  200,  100,  50,   100,  50,   50,   50,   200,  0,    100,  200,  200,  50],
    "POISON"    : [100, 100,  100,  100,  200,  100,  100,  50,   50,   100,  100,  100,  50,   50,   100,  100,  0,    200],
    "GROUND"    : [100, 200,  100,  200,  50,   100,  100,  200,  100,  0,    100,  50,   200,  100,  100,  100,  200,  100],
    "FLYING"    : [100, 100,  100,  50,   200,  100,  200,  100,  100,  100,  100,  200,  50,   100,  100,  100,  50,   100],
    "PSYCHIC"   : [100, 100,  100,  100,  100,  100,  200,  200,  100,  100,  50,   100,  100,  100,  100,  0,    50,   100],
    "BUG"       : [100, 50,   100,  100,  200,  100,  50,   50,   100,  50,   200,  100,  100,  50,   100,  200,  50,   50],
    "ROCK"      : [100, 200,  100,  100,  100,  200,  50,   100,  50,   200,  100,  200,  100,  100,  100,  100,  50,   100],
    "GHOST"     : [0,   100,  100,  100,  100,  100,  100,  100,  100,  100,  200,  100,  100,  200,  200,  50,   100,  100],
    "DRAGON"    : [100, 100,  100,  100,  100,  100,  100,  100,  100,  100,  100,  100,  100,  100,  100,  100,  50,   0],
    "DARK"      : [100, 100,  100,  100,  100,  100,  50,   100,  100,  100,  200,  100,  100,  200,  100,  50,   100,  50],
    "STEEL"     : [100, 50,   50,   50,   100,  200,  100,  100,  100,  100,  100,  100,  200,  100,  100,  100,  50,   200],
    "FAIRY"     : [100, 50,   100,  100,  100,  100,  200,  50,   100,  100,  100,  100,  100,  100,  200,  200,  50,   100],

  };

  var typeIndex = {
    "NORMAL"    : 0,
    "FIRE"      : 1,
    "WATER"     : 2,
    "ELECTRIC"  : 3,
    "GRASS"     : 4,
    "ICE"       : 5,
    "FIGHTING"  : 6,
    "POISON"    : 7,
    "GROUND"    : 8,
    "FLYING"    : 9,
    "PSYCHIC"   : 10,
    "BUG"       : 11,
    "ROCK"      : 12,
    "GHOST"     : 13,
    "DRAGON"    : 14,
    "DARK"      : 15,
    "STEEL"     : 16,
    "FAIRY"     : 17
  };

  function activate()
  {
    pokeDexService.getPokemon().get({id:$scope.items[0].id}).$promise.then(function(data)
    {
      if(data)
      {
        $scope.one = data;
        if(data.descriptions && data.descriptions.length>0)
        {
          var desc = data.descriptions[0];
          pokeDexService.getRes(desc.resource_uri).get().$promise.then(function(data)
          {
            if(data)
            {
              $scope.oneDesc = data;
            }
          });
          $scope.one.statRating = calculateStatRating(data);
        }

      }
    });

    pokeDexService.getPokemon().get({id:$scope.items[1].id}).$promise.then(function(data)
    {
      if(data)
      {
        $scope.two = data;
        if(data.descriptions && data.descriptions.length>0)
        {
          var desc = data.descriptions[0];
          pokeDexService.getRes(desc.resource_uri).get().$promise.then(function(data)
          {
            if(data)
            {
              $scope.twoDesc = data;
            }
          });
          $scope.two.statRating = calculateStatRating(data);
        }

      }
    });
  }

  function calculateStatRating(pokeMon)
  {
    /*hp - 255
    attack - 190
    defence - 230
    sp atck = 194
    sp def - 230
    speed - 180*/


    var speed = 0;
    if(pokeMon.speed)
    {
      speed = (pokeMon.speed/180)*100*0.167;
    }
    var attack = 0;
    if(pokeMon.speed)
    {
      attack = (pokeMon.attack/190)*100*0.167;
    }
    var defence = 0;
    if(pokeMon.defence)
    {
      defence = (pokeMon.defence/230)*100*0.167;
    }
    var sp_atk = 0;
    if(pokeMon.sp_atk)
    {
      sp_atk = (pokeMon.sp_atk/194)*100*0.167;
    }
    var sp_def = 0;
    if(pokeMon.sp_def)
    {
      sp_def = (pokeMon.sp_def/230)*100*0.167;
    }
    var hp = 0;
    if(pokeMon.hp)
    {
      hp = (pokeMon.hp/255)*100*0.167;
    }



    return Math.ceil(hp+sp_def+sp_atk+defence+attack+speed);
  }

  function calcTypeScore(self,opponent)
  {

    var score = "not Available";
    if(self && self.types && self.types.length>0)
    {
      score = 0;
      self.types.forEach(function(item,index)
      {
        var selfType = item.name.toUpperCase();
        if(opponent && opponent.types && opponent.types.length>0) {
          opponent.types.forEach(function (item, index) {
            var opponentType = item.name.toUpperCase();
            var tempArray = typeCalcMatrix[selfType];
            score += tempArray[typeIndex[opponentType]];
          });
        }

      });
      self.typeScore = score;
    }

    return score;
  }

  function ok() {
    $uibModalInstance.close();
  };

}])

.controller('PokemonDetailModalInstanceCtrl', ['$scope', '$uibModalInstance', 'item','pokeDexService',function($scope, $uibModalInstance, item, pokeDexService) {

      $scope.item = item;
      $scope.one;
      $scope.oneDesc ;

      $scope.ok = ok;

      activate();

      //////////////////////


      function activate()
      {
        pokeDexService.getPokemon().get({id:$scope.item.id}).$promise.then(function(data)
        {
          if(data)
          {
            $scope.one = data;
            if(data.descriptions && data.descriptions.length>0)
            {
              var desc = data.descriptions[0];
              pokeDexService.getRes(desc.resource_uri).get().$promise.then(function(data)
              {
                if(data)
                {
                  $scope.oneDesc = data;
                }
              });
              $scope.one.statRating = calculateStatRating(data);
            }

          }
        });
      }

      function calculateStatRating(pokeMon)
      {
        /*hp - 255
         attack - 190
         defence - 230
         sp atck = 194
         sp def - 230
         speed - 180*/


        var speed = 0;
        if(pokeMon.speed)
        {
          speed = (pokeMon.speed/180)*100*0.167;
        }
        var attack = 0;
        if(pokeMon.speed)
        {
          attack = (pokeMon.attack/190)*100*0.167;
        }
        var defence = 0;
        if(pokeMon.defence)
        {
          defence = (pokeMon.defence/230)*100*0.167;
        }
        var sp_atk = 0;
        if(pokeMon.sp_atk)
        {
          sp_atk = (pokeMon.sp_atk/194)*100*0.167;
        }
        var sp_def = 0;
        if(pokeMon.sp_def)
        {
          sp_def = (pokeMon.sp_def/230)*100*0.167;
        }
        var hp = 0;
        if(pokeMon.hp)
        {
          hp = (pokeMon.hp/255)*100*0.167;
        }
        return Math.ceil(hp+sp_def+sp_atk+defence+attack+speed);
      }


      function ok() {
        $uibModalInstance.close();
      };

    }]);