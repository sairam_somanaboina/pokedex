/**
 * Created by sairam on 03/01/16.
 */


'use strict';

angular.module('myApp.services.pokeDexService', ['ngResource'])

    .service('pokeDexService', ['$resource',function($resource) {


        this.getPokeDex = getPokeDex;
        this.getPokemon = getPokemon;
        this.getRes = getRes;


        function getPokeDex()
        {
             return $resource('http://pokeapi.co/api/v1/pokedex/1/');
        }

        function getPokemon()
        {
            return $resource('http://pokeapi.co/api/v1/pokemon/:id/',{id: "@id"});
        }

        function getRes(url)
        {
            return $resource('http://pokeapi.co/'+url);
        }



    }]);
